package sparkdeneme;

import java.io.IOException;

/**
 * Created by bugracakmak on 10/08/15.
 */

public class Main {
    public static void main(String[] args) {
        try {
            new Cli(args).parse();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
