package sparkdeneme;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Objects;

import javax.servlet.Servlet;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import spark.Request;
import spark.Response;
import spark.Route;


public class EncoderController implements Route {
	@Override
	public Object handle(Request request, Response response) throws Exception {
		HttpServletRequest raw = request.raw();
        StringBuilder sb = new StringBuilder();
        boolean isFile=true;
		ServletFileUpload upload = new ServletFileUpload();
        byte[] tempByteArray = new byte[5];
		FileItemIterator fileIter = upload.getItemIterator(raw);
		while (fileIter.hasNext()) {
		    FileItemStream item = fileIter.next();
		    InputStream stream = item.openStream();
            BufferedInputStream bst = new BufferedInputStream(stream);
            int size;
			if (!Objects.equals(item.getFieldName(), "uploadedfile")) {
                isFile=false;
				while ((size = bst.read(tempByteArray, 0, 5)) != -1) {
					sb.append(Base29.Encode(Base29.byteToLong(tempByteArray, size)));
				}
			}else{
                response.type("application/octet-stream");
                response.header("Content-Disposition", "attachment; filename=test");
                OutputStream out =response.raw().getOutputStream();
                try {
                    while ((size = bst.read(tempByteArray, 0, 5)) != -1) {
                        String temp = Base29.Encode(Base29.byteToLong(tempByteArray, size));
                        out.write(temp.getBytes());
                    }
                }finally {
                    out.close();
                }

			}
		}


        if(!isFile)
		    return sb.toString();
        else
            return "";

	}
}
