package sparkdeneme;

import spark.Spark;

import java.io.FileNotFoundException;

public class ApplicationServer {

	public static void main(String[] args)  {
        Spark.staticFileLocation("/public");
        if(args.length != 0)
            Spark.port(Integer.parseInt(args[0]));
		Spark.post("/encode", new EncoderController());
		Spark.post("/decode", new DecoderController());
		

	}


}
