package sparkdeneme;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;



public class Cli {
    private String[] args = null;
    private Options options = new Options();
    private static final Logger log = Logger.getLogger(Cli.class.getName());

    public Cli(String[] args) {

        this.args = args;

        options.addOption("h", "help", false, "show help.");
        options.addOption("s", "server", false, "web server mode");
        options.addOption("p", "port", true, "port number of the web server");
        options.addOption("e","encode", false, "encode mode" );
        options.addOption("d", "decode", false, "decode mode");
        options.addOption("F", "file", true, "file mode");
        options.addOption("S", "string", true, "string mode");


    }

    public void parse() throws IOException {
        CommandLineParser parser = new BasicParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);

            if (cmd.hasOption("h"))
                help();
            else if (cmd.hasOption("s")) {
                if(cmd.hasOption("p")){
                    ApplicationServer.main(new String[]{cmd.getOptionValue("p")});
                }
                else{
                    ApplicationServer.main(new String[]{"8080"});
                }
            }
            else if(cmd.hasOption("e")){
                if(cmd.hasOption("S")){
                    System.out.println(Base29.Encode(cmd.getOptionValue("S")));
                }
                else if(cmd.hasOption("F")){
                    String fileName=cmd.getOptionValue("F");
                    System.out.println("Encoding file "+fileName);
                    try {
                        Base29.EncodeFile(fileName);
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    help();
                }
            }
            else if(cmd.hasOption("d")){
                if(cmd.hasOption("S")){
                    StringBuilder sb = new StringBuilder();
                    String decodedString=cmd.getOptionValue("S");
                    int i=0,j=0;
                    String tempString;
                    while (i+9 < decodedString.length()){
                        int start = j*9;
                        int end = start +9;

                        if(start+8>decodedString.length())
                            end=decodedString.length()-1;

                        tempString=decodedString.substring(start, end);
                        sb.append(Base29.Decode(tempString));
                        i=i+9;
                        j++;
                    }
                    sb.append(Base29.Decode(decodedString.substring(i,decodedString.length())));

                    System.out.println(sb.toString());
                }
                else if(cmd.hasOption("F")){
                    String fileName=cmd.getOptionValue("F");
                    System.out.println("Encoding file "+fileName);
                    try {
                        Base29.DecodeFile(fileName);
                    } catch (java.io.IOException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    help();
                }
            }
            else {
                help();
            }

        } catch (ParseException e) {
            help();
        }
    }

    private void help() {
        HelpFormatter formater = new HelpFormatter();
        formater.printHelp("Main", options);
        System.exit(0);
    }
}
