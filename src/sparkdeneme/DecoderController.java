package sparkdeneme;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import spark.Request;
import spark.Response;
import spark.Route;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

/**
 * Created by bugra on 08.08.2015.
 */
public class DecoderController implements Route {
    @Override
    public Object handle(Request request, Response response) throws Exception {
        HttpServletRequest raw = request.raw();
        StringBuilder sb = new StringBuilder();
        String byteString;
        int size;
        ServletFileUpload upload = new ServletFileUpload();
        byte[] tempByteArray = new byte[9];
        boolean isFile=true;
        FileItemIterator fileIter = upload.getItemIterator(raw);
        while (fileIter.hasNext()) {
            FileItemStream item = fileIter.next();
            InputStream stream = item.openStream();
            BufferedInputStream bst = new BufferedInputStream(stream);
            if (!Objects.equals(item.getFieldName(), "uploadedfile")) {
                isFile=false;
                while((size = bst.read(tempByteArray,0,9)) != -1){
                    byteString = new String(tempByteArray, StandardCharsets.US_ASCII);
                    sb.append(Base29.Decode(byteString));
                    tempByteArray = new byte[9];
                }
            } else {
                response.type("application/octet-stream");
                response.header("Content-Disposition", "attachment; filename=test");
                ServletOutputStream out = response.raw().getOutputStream();
                while ((size = bst.read(tempByteArray, 0, 9)) != -1) {
                    byteString = new String(tempByteArray, StandardCharsets.US_ASCII);
                    byte[] temp = new byte[5];
                    temp =Base29.DecodetoByte(byteString) ;
                    int i=0;
                    byte[] oneByte = new byte[1];
                    while(i<temp.length){
                        if(oneByte[0] == temp[i]){
                            break;
                        }
                        i++;
                    }
                    out.write(temp,0,i);
                }
                out.close();
            }

        }
        if(!isFile)
            return sb.toString();
        else
            return "";
    }
}
