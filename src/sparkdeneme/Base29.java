package sparkdeneme;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * Created by bugra on 08.08.2015.
 */
public class Base29 {
    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    final protected static char[] code = "abcdefghijklmnopqrstuvwxyz123".toCharArray();
    protected static final Map<Character, Integer> valueTable=createMap();

    private static Map<Character, Integer> createMap()
    {
        Map<Character, Integer> checkSum = new HashMap<Character, Integer>();

        for ( int i = 0; i < code.length; ++i ) {
            checkSum.put(code[i], i);
        }

        return Collections.unmodifiableMap(checkSum);
    }
    public static void EncodeFile(String filename) throws IOException {
        BufferedInputStream bis;
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filename+".out"));
        byte[] tempByteArray = new byte[5];
        try {
            bis = new BufferedInputStream(new FileInputStream(filename));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }
        int size;
        try {
            while ((size = bis.read(tempByteArray, 0, 5)) != -1) {
                String temp = Base29.Encode(Base29.byteToLong(tempByteArray, size));
                bos.write(temp.getBytes());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        bos.close();
    }
    public static void DecodeFile(String filename) throws IOException {
        BufferedInputStream bis;
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filename+".decoded"));
        byte[] tempByteArray = new byte[9];
        try {
            bis = new BufferedInputStream(new FileInputStream(filename));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }
        int size;
        try {
            while ((size = bis.read(tempByteArray, 0, 9)) != -1) {

                String byteString = new String(tempByteArray, StandardCharsets.US_ASCII);
                String[] parts = byteString.split("a");
                byte[] temp = new byte[5];

                temp =Base29.DecodetoByte(parts[0]) ;
                int i=0;
                byte[] oneByte = new byte[1];
                while(i<temp.length){
                    if(oneByte[0] == temp[i]){
                        break;
                    }
                    i++;
                }
                bos.write(temp,0,i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        bos.close();
    }
    public static String Encode(long input)
    {
        boolean flag = true;
        StringBuilder text = new StringBuilder();
        while(input>0){
            long y = (int) (input%29);
            input=input/29;
            text.append(code[(int) y]);
        }
        while (text.length()<9  && flag){
            if(flag) {
                text.append("a");
                flag=false;
            }
        }
        return text.toString();
    }


    public static String Decode(String input){
        long sum = 0L;
        StringBuffer sb = new StringBuffer();
        boolean flag = true;
        int i=0;
        for (char c: input.toCharArray()) {
            if (c !='\u0000') {
                sum = sum + (long) valueTable.get(c) * (long) Math.pow(29, i);
                i++;
            }
        }
        sb.append(convertByteArraytoString(longToByte(sum)));
        return sb.toString();
    }
    public static byte[] DecodetoByte(String input){
        long sum = 0L;
        StringBuffer sb = new StringBuffer();
        byte[] result = new byte[5];
        boolean flag = true;
        int i=0;
        for (char c: input.toCharArray()) {
            if (c !='\u0000') {
                sum = sum + (long) valueTable.get(c) * (long) Math.pow(29, i);
                i++;
            }
        }
        result=longToByte(sum);
        return result;
    }


    public static String convertByteArraytoString(byte[] data) {
        StringBuilder sb = new StringBuilder(data.length);
        for (int i = 0; i < data.length; ++ i) {
            if (data[i] < 0) {
                data[i] = (byte) (data[i] + 256);
            }
            if (data[i]>0)
                sb.append((char) data[i]);
        }
        return sb.toString();
    }
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
    public static String Encode(String input){
        StringBuilder sb = new StringBuilder();
        byte[] b = input.getBytes(StandardCharsets.US_ASCII);
        byte[] tempByteArray = new byte[5];
        ByteArrayInputStream bis = new ByteArrayInputStream(b);
        int size;
        while((size = bis.read(tempByteArray,0,5)) != -1){
            sb.append(Base29.Encode(Base29.byteToLong(tempByteArray, size)));
        }
       return sb.toString();
    }

    public static byte[] longToByte(long input){
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.putLong(Long.reverseBytes(input));
        byte[] longInBytes = new byte[8];
        buffer.rewind();
        buffer.get(longInBytes);
        return longInBytes;
    }

    public static long byteToLong(byte[] input,int size){
        long result=0;
        for (int i = 0; i < 5; i++) {
            if (size>i) {
                if (input[i] < 0) {
                    result = result + (long) ((input[i] + 256) * Math.pow(256, i));
                } else {
                    result = result + (long) (input[i] * Math.pow(256, i));
                }
            }
        }
        return result;
    }

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
