$(document).ready(function() {
    var st = true;
    $("#chk").bootstrapSwitch({
        onText: 'TEXT',
        offText: 'FILE'
    });
    $('#encode').click(function() {
        var fd = new FormData();

        if (!st) {
            $.each($('#chkfile')[0].files, function(i, file) {
                fd.append('uploadedfile', file);
            });
            $( "#fileupload" ).submit();

        } else {
            fd.append('text', $("#name").val());
            $.ajax({
                url: '/encode',
                data: fd,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(data) {
                    $("#name").val(data);
                }
            });
        }


    })

    $('#decode').click(function() {

        var fd = new FormData();
        if (!st) {
            $.each($('#chkfile')[0].files, function(i, file) {
                fd.append('uploadedfile', file);
            });
            $( "#fileupload" ).submit();


        } else {
            fd.append('text', $("#name").val());
        }

        $.ajax({
            url: '/decode',
            data: fd,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function(data) {
                $("#name").val(data);
            }
        });
    })

    $('#chk').on('switchChange.bootstrapSwitch', function(event, state) {
        st = state;
        if (state) {
            $("#chkfile").css('display', 'none');
            $("#name").css('display', 'block');
            $("#result").css('display', 'none');
        } else {
            $("#chkfile").css('display', 'block');
            $("#name").css('display', 'none');
            $("#result").css('display', 'block');
        }
    })
});
function ajax_download(url, data, input_name) {
    var $iframe,
        iframe_doc,
        iframe_html;

    if (($iframe = $('#download_iframe')).length === 0) {
        $iframe = $("<iframe id='download_iframe'" +
            " style='display: none' src='about:blank'></iframe>"
        ).appendTo("body");
    }

    iframe_doc = $iframe[0].contentWindow || $iframe[0].contentDocument;
    if (iframe_doc.document) {
        iframe_doc = iframe_doc.document;
    }

    iframe_html = "<html><head></head><body><form method='POST' action='" +
    url +"'>" +
    "<input type=hidden name='" + input_name + "' value='" +
    JSON.stringify(data) +"'/></form>" +
    "</body></html>";

    iframe_doc.open();
    iframe_doc.write(iframe_html);
    $(iframe_doc).find('form').submit();
}